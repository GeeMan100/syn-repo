//
//  GameScene.m
//  Syn
//
//  Created by Gurmit Singh on 23/08/2014.
//  Copyright (c) 2014 RuleOnSix. All rights reserved.
//

//TODO: highest y value is 240 up -30 is 210 - 30 is 180 - 30 is 150
//TODO: check for colours that go together
//TODO: insert more data into Core Data

#import "GameScene.h"
#import "AppDelegate.h"
#import "CurrentProgressOfGame.h"
static int timeCount = 60;
int capacitySynonym = 0;
int capacityAntonym = 0;
@interface GameScene()
{
    SKLabelNode *antonym1, *antonym2, *antonym3, *antonym4, *synonym1, *synonym2, *synonym3, *synonym4, *words1, *words2, *words3, *words4, *words5, *title, *timeLabel, *antonymName, *synonymName, *finishLabel;
    SKShapeNode* antonymBox;
    SKShapeNode* synonymBox;
    SKShapeNode* outOfTheBoxes;
    //SKShapeNode* middleOfBoxes;
    CGFloat screenWidth;
    CGFloat screenHeight;
    SKNode *node, *nodeBeginPosition;
    SKNode *insideSynonym1;
    SKNode *insideAntonym1;
    SKNode *insideSynonym2;
    SKNode *insideAntonym2;
    BOOL beginningCurrentNumber;
    SKNode *insideSynonym3;
    SKNode *insideAntonym3;
    SKNode *insideSynonym4;
    SKNode *insideAntonym4;
    
    //outsideOfTheBoxesLeft
    SKNode *outsideOfTheBoxesLeft1;
    SKNode *outsideOfTheBoxesLeft2;
    SKNode *outsideOfTheBoxesLeft3;
    SKNode *outsideOfTheBoxesLeft4;
    SKNode *outsideOfTheBoxesLeft5;
    //outsideOfTheBoxesMiddle;
    
    SKNode *outsideOfTheBoxesMiddle1;
    SKNode *outsideOfTheBoxesMiddle2;
    SKNode *outsideOfTheBoxesMiddle3;
    SKNode *outsideOfTheBoxesMiddle4;
    SKNode *outsideOfTheBoxesMiddle5;
    
    //outsideOfTheBoxesRight
    
    SKNode *outsideOfTheBoxesRight1;
    SKNode *outsideOfTheBoxesRight2;
    SKNode *outsideOfTheBoxesRight3;
    SKNode *outsideOfTheBoxesRight4;
    SKNode *outsideOfTheBoxesRight5;
    
    SKLabelNode *positionLabel;
    SKLabelNode *howManyAnts, *howManySyns;
    CGPoint beginLocation;
    NSArray *fetchedObjects;
    long currentNumber;
    BOOL nodeMoved;
    dispatch_queue_t myQueue;
    dispatch_queue_t myQueue2;
}
@end
@implementation GameScene
@synthesize managedObjectContext = _managedObjectContext;
@synthesize fetchedResultsController = _fetchedResultsController;
#pragma mark - how many ants
-(void)howManyAntsToAnswer{
    howManyAnts = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    
    //title.text = @"Word: Flight - adjective";
    howManyAnts.fontSize = 15;
    howManyAnts.position = CGPointMake(screenWidth/2 - 50,
                                                  screenHeight-37);
    howManyAnts.name = @"howManyAnts";
    howManyAnts.zPosition = -20;
    howManyAnts.color = [SKColor blackColor];
    howManyAnts.colorBlendFactor = 1;
    [self addChild:howManyAnts];
    
}
#pragma mark - how many syns
-(void)howManySynsToAnswer{
    howManySyns = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    
    //title.text = @"Word: Flight - adjective";
    howManySyns.fontSize = 15;
    howManySyns.position = CGPointMake(screenWidth - 50,
                                       screenHeight- 37);
    howManySyns.name = @"howManySyns";
    howManySyns.zPosition = -20;
    howManySyns.color = [SKColor blackColor];
    howManySyns.colorBlendFactor = 1;
    [self addChild:howManySyns];
    
}
#pragma mark - show position of labelnodes
-(void)positionTheLabel{
    positionLabel = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    
    //title.text = @"Word: Flight - adjective";
    positionLabel.fontSize = 15;
    positionLabel.position = CGPointMake(screenWidth/2 + 30,
                                 screenHeight/2 - 40);
    positionLabel.name = @"title";
    positionLabel.zPosition = -20;
    positionLabel.color = [SKColor blackColor];
    positionLabel.colorBlendFactor = 1;
    [self addChild:positionLabel];

}
-(void)addTheTitle{
    title = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    
    //title.text = @"Word: Flight - adjective";
    title.fontSize = 15;
    title.position = CGPointMake(screenWidth/2,
                                 screenHeight - 20);
    title.name = @"title";
    title.color = [SKColor redColor];
    title.colorBlendFactor = 1;
    [self addChild:title];
}
#pragma mark - synonyms
//synonym4
- (void)addSynonym4 {
    synonym4 = [SKLabelNode labelNodeWithFontNamed:@"Ariel"];
    
    // antonym1.text = @"Run";
    synonym4.fontSize = 30;
    synonym4.position = CGPointMake(100,
                                    screenHeight/2 - 100);
    synonym4.name = @"synonym4";
    synonym4.color = [SKColor greenColor];
    synonym4.colorBlendFactor = 1;
    [self addChild:synonym4];
}
// synonym3
- (void)addSynonym3 {
    synonym3 = [SKLabelNode labelNodeWithFontNamed:@"Ariel"];
    
    // antonym1.text = @"Run";
    synonym3.fontSize = 28;
    synonym3.position = CGPointMake(80,
                                    screenHeight/2 - 90);
    synonym3.name = @"synonym3";
    synonym3.color = [SKColor greenColor];
    synonym3.colorBlendFactor = 1;
    [self addChild:synonym3];
}

// synonym2
- (void)addSynonym2 {
    synonym2 = [SKLabelNode labelNodeWithFontNamed:@"Ariel"];
    
    // antonym1.text = @"Run";
    synonym2.fontSize = 28;
    synonym2.position = CGPointMake(screenWidth/2 + 50,
                                    screenHeight/2 - 80);
    synonym2.name = @"synonym2";
    synonym2.color = [SKColor greenColor];
    synonym2.colorBlendFactor = 1;
    [self addChild:synonym2];
}
// synonym1
- (void)addSynonym1 {
    synonym1 = [SKLabelNode labelNodeWithFontNamed:@"Ariel"];
    
    // antonym1.text = @"Run";
    synonym1.fontSize = 28;
    synonym1.position = CGPointMake(screenWidth/2 + 40,
                                    screenHeight/2 - 40);
    synonym1.name = @"synonym1";
    synonym1.color = [SKColor greenColor];
    synonym1.colorBlendFactor = 1;
    [self addChild:synonym1];
}
#pragma mark - words
//words5
- (void)addWords5 {
    words5 = [SKLabelNode labelNodeWithFontNamed:@"Ariel"];
    
    // words4.text = @"Run";
    words5.fontSize = 30;
    words5.position = CGPointMake(160,
                                  screenHeight/2 - 100);
    words5.name = @"words4";
    words5.color = [SKColor greenColor];
    words5.colorBlendFactor = 1;
    [self addChild:words5];
}

//words4
- (void)addWords4 {
    words4 = [SKLabelNode labelNodeWithFontNamed:@"Ariel"];
    
    // words4.text = @"Run";
    words4.fontSize = 30;
    words4.position = CGPointMake(130,
                                  screenHeight/2 - 100);
    words4.name = @"words4";
    words4.color = [SKColor greenColor];
    words4.colorBlendFactor = 1;
    [self addChild:words4];
}
// words3
- (void)addWords3 {
    words3 = [SKLabelNode labelNodeWithFontNamed:@"Ariel"];
    
    // antonym1.text = @"Run";
    words3.fontSize = 28;
    words3.position = CGPointMake(screenWidth -90,
                                  screenHeight/2 - 90);
    words3.name = @"words3";
    words3.color = [SKColor greenColor];
    words3.colorBlendFactor = 1;
    [self addChild:words3];
}

// words2
- (void)addWords2 {
    words2 = [SKLabelNode labelNodeWithFontNamed:@"Ariel"];
    
    // antonym1.text = @"Run";
    words2.fontSize = 28;
    words2.position = CGPointMake(screenWidth/2 - 50,
                                  screenHeight/2 - 80);
    words2.name = @"words2";
    words2.color = [SKColor greenColor];
    words2.colorBlendFactor = 1;
    [self addChild:words2];
}
// words1
- (void)addWords1 {
    words1 = [SKLabelNode labelNodeWithFontNamed:@"Ariel"];
    
    // antonym1.text = @"Run";
    words1.fontSize = 28;
    words1.position = CGPointMake(screenWidth - 100,
                                  screenHeight/2 - 60);
    words1.name = @"words1";
    words1.color = [SKColor greenColor];
    words1.colorBlendFactor = 1;
    [self addChild:words1];
}
- (void)addAntonym1 {
    antonym1 = [SKLabelNode labelNodeWithFontNamed:@"Ariel"];
    
    // antonym1.text = @"Run";
    antonym1.fontSize = 28;
    antonym1.position = CGPointMake(screenWidth/2,
                                    screenHeight/2 - 50);
    antonym1.name = @"antonym1";
    antonym1.color = [SKColor greenColor];
    antonym1.colorBlendFactor = 1;
    [self addChild:antonym1];
}
//finish label
- (void)addFinishLabel {
    finishLabel = [SKLabelNode labelNodeWithFontNamed:@"Ariel"];
    
    finishLabel.text = @">>finish";
    finishLabel.fontSize = 22;
    finishLabel.position = CGPointMake(45,
                                       screenHeight - 20);
    finishLabel.name = @"finish";
    finishLabel.color = [SKColor greenColor];
    finishLabel.colorBlendFactor = 1;
    [self addChild:finishLabel];
}
- (void)addAntonym2 {
    antonym2 = [SKLabelNode labelNodeWithFontNamed:@"Ariel"];
    
    //myThirdLabel.text = @"travel";
    antonym2.fontSize = 28;
    antonym2.position = CGPointMake(screenWidth/2 + 60,
                                    screenHeight/2 - 60);
    antonym2.name = @"antonyms";
    antonym2.color = [SKColor greenColor];
    antonym2.colorBlendFactor = 1;
    [self addChild:antonym2];
}
//antonym3
- (void)addAntonym3 {
    antonym3 = [SKLabelNode labelNodeWithFontNamed:@"Ariel"];
    
    //myThirdLabel.text = @"travel";
    antonym3.fontSize = 28;
    antonym3.position = CGPointMake(screenWidth/2 - 20,
                                    screenHeight/2 - 90);
    antonym3.name = @"antonyms";
    antonym3.color = [SKColor greenColor];
    antonym3.colorBlendFactor = 1;
    [self addChild:antonym3];
}
- (void)addAntonym4 {
    antonym4 = [SKLabelNode labelNodeWithFontNamed:@"Ariel"];
    
    //myThirdLabel.text = @"travel";
    antonym4.fontSize = 28;
    antonym4.position = CGPointMake(screenWidth/2 - 40,
                                    screenHeight/2 - 50);
    antonym4.name = @"antonyms";
    antonym4.color = [SKColor greenColor];
    antonym4.colorBlendFactor = 1;
    [self addChild:antonym4];
}
- (void)displayTheTime {
    timeLabel = [SKLabelNode labelNodeWithFontNamed:@"Ariel"];
    
    timeLabel.text = [NSString stringWithFormat:@"%d",timeCount];
    timeLabel.fontSize = 20;
    timeLabel.position = CGPointMake(screenWidth-25,
                                     screenHeight-20);
    timeLabel.name = @"timeLabel";
    timeLabel.color = [SKColor redColor];
    timeLabel.colorBlendFactor = 1;
    [self addChild:timeLabel];
}
-(void)addTheNodes{
    [self howManyAntsToAnswer];
    [self howManySynsToAnswer];

    [self addTheTitle];
    [self addAntonym1];
    
    [self addAntonym2];
    [self addAntonym3];
    [self addAntonym4];
    // add the synonyms
    [self addSynonym1];
    
    [self addSynonym2];
    [self addSynonym3];
    [self addSynonym4];
    // add the words
    [self addWords1];
    
    [self addWords2];
    [self addWords3];
    [self addWords4];
    [self addWords5];
    [self positionTheLabel]; //*** delete this
}
-(void)didMoveToView:(SKView *)view {
    /* Setup your scene here */
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    screenWidth = screenRect.size.width;
    screenHeight = screenRect.size.height;
    //NSLog(@"screenheight = %f", screenHeight);
    self.scene.backgroundColor = [SKColor blackColor];
    capacitySynonym = 0;
    capacityAntonym = 0;
    
    [self displayTheTime];
    [self addFinishLabel];
    
    [self addTheInsideBoxesNodes];
    [self addTheOutsideOfTheBoxesNodes];
    // middleoftheboxes
    //outoftheboxes
//    middleOfBoxes = [SKShapeNode node];
//    middleOfBoxes.name = @"outOfTheBoxes";
//    [middleOfBoxes setPath:CGPathCreateWithRoundedRect(CGRectMake(screenWidth/2-10, screenHeight/2-10, 30, screenHeight/2 -20), 10, 10, nil)];
//    
//    middleOfBoxes.strokeColor = middleOfBoxes.fillColor = [UIColor colorWithRed:255.0
//                                                                          green:0.0
//                                                                           blue:0.0
//                                                                          alpha:1.0];
//    middleOfBoxes.zPosition = -10;
//    //antonymBox.name = @"antonymBox";
//    [self addChild: middleOfBoxes];
    //
    //outoftheboxes
    outOfTheBoxes = [SKShapeNode node];
    outOfTheBoxes.name = @"outOfTheBoxes";
    [outOfTheBoxes setPath:CGPathCreateWithRoundedRect(CGRectMake(0, 0, screenWidth, screenHeight), 10, 10, nil)];
    beginningCurrentNumber = YES;
    outOfTheBoxes.strokeColor = outOfTheBoxes.fillColor = [UIColor colorWithRed:255.0
                                                                    green:255.0
                                                                     blue:255.0
                                                                    alpha:1.0];
    outOfTheBoxes.zPosition = -20;
    //antonymBox.name = @"antonymBox";
    [self addChild: outOfTheBoxes];
    //
    antonymBox = [SKShapeNode node];
    antonymBox.name = @"antonymBox";
    [antonymBox setPath:CGPathCreateWithRoundedRect(CGRectMake(10, screenHeight/2, (screenWidth/2) -10, screenHeight/2 - 40), 10, 10, nil)];
    antonymBox.strokeColor = antonymBox.fillColor = [UIColor colorWithRed:0.0/255.0
                                                                    green:128.0/255.0
                                                                     blue:255.0/255.0
                                                                    alpha:1.0];
    antonymBox.zPosition = -10;
    //antonymBox.name = @"antonymBox";
    [self addChild: antonymBox];
    
    antonymName = [SKLabelNode labelNodeWithFontNamed:@"Ariel"];
    
    antonymName.text = @"Antonyms";
    antonymName.fontSize = 18;
    antonymName.position = CGPointMake(55,
                                       screenHeight-37);
    antonymName.name = @"antonymName";
    antonymName.color = [SKColor blackColor];
    antonymName.colorBlendFactor = 1;
    antonymName.zPosition = 4;
    [self addChild:antonymName];
    
    synonymBox = [SKShapeNode node];
    [synonymBox setPath:CGPathCreateWithRoundedRect(CGRectMake(screenWidth/2 + 10, screenHeight/2, (screenWidth/2 -20), screenHeight/2 - 40), 10, 10, nil)];
    synonymBox.strokeColor = synonymBox.fillColor = [UIColor colorWithRed:0.0/255.0
                                                                    green:128.0/255.0
                                                                     blue:255.0/255.0
                                                                    alpha:1.0];
    synonymBox.zPosition = -10;
    synonymBox.name = @"synonymBox";
    [self addChild: synonymBox];
    
    synonymName = [SKLabelNode labelNodeWithFontNamed:@"Ariel"];
    
    synonymName.text = @"Synonyms";
    synonymName.fontSize = 18;
    synonymName.position = CGPointMake(screenWidth/2 + 55,
                                       screenHeight-37);
    synonymName.name = @"synonymName";
    synonymName.color = [SKColor blackColor];
    synonymName.colorBlendFactor = 1;
    synonymName.zPosition = 4;
    [self addChild:synonymName];
    
    //set the time count
    timeCount = 60;
    
    //share the appdelegate and managedobjectcontext
    id delegate = [[UIApplication sharedApplication] delegate];
    self.managedObjectContext = [delegate managedObjectContext];
    currentNumber = [self getCount];
    if (currentNumber == 0) {
        [self insertTheData];
    }
    
    
    //currentNumber = 0;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"myCurrentGame"] == nil) {
        [defaults setObject:@"0" forKey:@"myCurrentGame"];
        [defaults synchronize];
    }
    
    NSString *getValue = [defaults objectForKey:@"myCurrentGame"];
    
    
    
    if ([getValue intValue] >= [self getCount]) {
        [defaults setObject:@"0" forKey:@"myCurrentGame"];
        [defaults synchronize];
    }
    getValue = [defaults objectForKey:@"myCurrentGame"];
    currentNumber = [getValue intValue];
    NSLog(@"gamescene [getValue intValue] = %d",[getValue intValue]);
    NSLog(@"currentNumber %ld", currentNumber);
    [self getValues];
    //currentNumber = 1;
    
}
-(void)insertTheData{
    currentNumber = 0;
    [self insertNewObject:@"chaos" :@"disarry" :@"untidiness" :nil :@"tidiness" :@"neatness" :@"trimness" :@"harmony" :@"sequence" :@"arrangement" :@"command" :@"instruction" :@"directive" :@"Order - Noun"];
    currentNumber = 1;
    [self insertNewObject:@"chaos" :@"disarry" :@"untidiness" :nil :@"tidiness" :@"neatness" :@"trimness" :@"harmony" :@"sequence" :@"arrangement" :@"command" :@"instruction" :@"directive" :@"Order - adjective"];
    
    
    currentNumber = 2;
    
    [self insertNewObject:@"untidiness" :nil :nil :nil :@"tidiness" :@"neatness" :@"trimness" :@"harmony" :@"sequence" :@"arrangement" :@"command" :@"instruction" :@"directive" :@"Order - adverb"];
}


- (void)insertNewObject:(NSString*)ant1 :(NSString*)ant2 :(NSString*)ant3 :(NSString*)ant4  :(NSString*)syn1 :(NSString*)syn2 :(NSString*)syn3 :(NSString*)syn4 :(NSString*)word1 :(NSString*)word2 :(NSString*)word3 :(NSString*)word4 :(NSString*)word5 :(NSString*)title1
{
    // If appropriate, configure the new managed object.
    // Normally you should use accessor methods, but using KVC here avoids the need to add a custom class to the template.
    NSManagedObject *newManagedObject = [NSEntityDescription insertNewObjectForEntityForName:@"Entity" inManagedObjectContext:self.managedObjectContext];
    [newManagedObject setValue:[NSDate date] forKey:@"timeStamp"];
    [newManagedObject setValue:title1 forKey:@"title"];
    [newManagedObject setValue:[NSNumber numberWithInteger:currentNumber] forKey:@"cardNumber"];
    [newManagedObject setValue:ant1 forKey:@"antonym1"];
    //if (ant2 != nil) {
    [newManagedObject setValue:ant2 forKey:@"antonym2"];
    // }
    // if (ant3 != nil) {
    [newManagedObject setValue:ant3 forKey:@"antonym3"];
    // }
    // if (ant4 != nil) {
    [newManagedObject setValue:ant4 forKey:@"antonym4"];
    // }
    [newManagedObject setValue:syn1 forKey:@"synonym1"];
    // if (syn2 != nil) {
    [newManagedObject setValue:syn2 forKey:@"synonym2"];
    //  }
    // if (syn3 != nil) {
    [newManagedObject setValue:syn3 forKey:@"synonym3"];
    // }
    // if (syn4 != nil) {
    [newManagedObject setValue:syn4 forKey:@"synonym4"];
    // }
    [newManagedObject setValue:word1 forKey:@"word1"];
    // if (word2 != nil) {
    [newManagedObject setValue:word2 forKey:@"word2"];
    // }
    // if (word3 != nil) {
    [newManagedObject setValue:word3 forKey:@"word3"];
    // }
    // if (word4 != nil) {
    [newManagedObject setValue:word4 forKey:@"word4"];
    // }
    // if (word5 != nil) {
    [newManagedObject setValue:word5 forKey:@"word5"];
    // }
    
    
    
    
    NSError *error = nil;
    if (![self.managedObjectContext save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
}
-(long)getCount{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    //  NSPersistentStoreCoordinator *ps = [_managedObjectContext persistentStoreCoordinator];
    // NSManagedObjectModel *model = [ps managedObjectModel];
    //NSFetchRequest *fetchRequest = [[model  fetchRequestTemplateForName:@"FetchCompleted"]copy];
    NSEntityDescription *entity2 = [NSEntityDescription entityForName:@"Entity" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity2];
    
    // Set the batch size to a suitable number.
    //[fetchRequest setFetchBatchSize:1000];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"cardNumber" ascending:YES];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    // if (![self.fetchedResultsController performFetch:&error]) {
    fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        // Handle the error
        //NSLog(@"fetchedObjects == nil");
    }
    //NSLog(@"fetchedObjects.count == %lu", (unsigned long)fetchedObjects.count);
    return fetchedObjects.count;
    
}
-(void)getValues{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    //  NSPersistentStoreCoordinator *ps = [_managedObjectContext persistentStoreCoordinator];
    // NSManagedObjectModel *model = [ps managedObjectModel];
    //NSFetchRequest *fetchRequest = [[model  fetchRequestTemplateForName:@"FetchCompleted"]copy];
    NSEntityDescription *entity2 = [NSEntityDescription entityForName:@"Entity" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity2];
    
    // Set the batch size to a suitable number.
    //[fetchRequest setFetchBatchSize:1000];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cardNumber == %d", currentNumber];
    [fetchRequest setPredicate:predicate];
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"cardNumber" ascending:YES];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    // if (![self.fetchedResultsController performFetch:&error]) {
    fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        // Handle the error
        //NSLog(@"fetchedObjects == nil");
    }
    if (fetchedObjects != nil) {
        //currentNumber = [self getCount] - 1;
       // NSLog(@"currentNumber before loop is = %ld", currentNumber);
        for(NSManagedObject* currentObj in fetchedObjects) {
            //if ([currentObj valueForKey:@"cardNumber"] == [NSNumber numberWithInteger:currentNumber]) {
            
            //            NSLog(@"date = %@", [currentObj valueForKey:@"timeStamp"]);
            //            NSLog(@"Print the cardNumber %@", [currentObj valueForKey:@"cardNumber"]);
            //            NSLog(@"ant1 = %@", [currentObj valueForKey:@"antonym1"]);
            //            NSLog(@"syn1 %@", [currentObj valueForKey:@"synonym1"]);
            //            NSLog(@"ant2 = %@", [currentObj valueForKey:@"antonym2"]);
            //            NSLog(@"syn2 %@", [currentObj valueForKey:@"synonym2"]);
            //            NSLog(@"ant3 = %@", [currentObj valueForKey:@"antonym3"]);
            //            NSLog(@"syn3 %@", [currentObj valueForKey:@"synonym3"]);
            //            NSLog(@"ant4 = %@", [currentObj valueForKey:@"antonym4"]);
            //            NSLog(@"syn4 %@", [currentObj valueForKey:@"synonym4"]);
            //            NSLog(@"word1 = %@", [currentObj valueForKey:@"word1"]);
            //            NSLog(@"word2 = %@", [currentObj valueForKey:@"word2"]);
            //            NSLog(@"word3 = %@", [currentObj valueForKey:@"word3"]);
            //            NSLog(@"word4 = %@", [currentObj valueForKey:@"word4"]);
            //            NSLog(@"word5 = %@", [currentObj valueForKey:@"word5"]);
            [self addTheNodes];
            
            if ([currentObj valueForKey:@"title"]!= nil)
            {
                title.text = [currentObj valueForKey:@"title"];
            }
            if ([currentObj valueForKey:@"antonym1"]!= nil) {
                antonym1.text = [currentObj valueForKey:@"antonym1"];
                howManyAnts.text = @"input 1";
                [self positionTheNodesOutsideOfTheBoxes];
            }
            if ([currentObj valueForKey:@"antonym2"]!= nil)
            {
                antonym2.text = [currentObj valueForKey:@"antonym2"];
                howManyAnts.text = @"input 2";
                [self positionTheNodesOutsideOfTheBoxes];
            }
            if ([currentObj valueForKey:@"antonym3"]!= nil)
            {
                antonym3.text = [currentObj valueForKey:@"antonym3"];
                howManyAnts.text = @"input 3";
                [self positionTheNodesOutsideOfTheBoxes];
            }
            if ([currentObj valueForKey:@"antonym4"]!= nil)
            {
                
                antonym4.text = [currentObj valueForKey:@"antonym4"];
                howManyAnts.text = @"input 4";
                [self positionTheNodesOutsideOfTheBoxes];
            }
            //add the synonyms
            if ([currentObj valueForKey:@"synonym1"]!= nil) {
                synonym1.text = [currentObj valueForKey:@"synonym1"];
                howManySyns.text = @"input 1";
                [self positionTheNodesOutsideOfTheBoxes];
            }
            if ([currentObj valueForKey:@"synonym2"]!= nil)
            {
                synonym2.text = [currentObj valueForKey:@"synonym2"];
                howManySyns.text = @"input 2";
                [self positionTheNodesOutsideOfTheBoxes];
            }
            if ([currentObj valueForKey:@"synonym3"]!= nil)
            {
                synonym3.text = [currentObj valueForKey:@"synonym3"];
                howManySyns.text = @"input 3";
                [self positionTheNodesOutsideOfTheBoxes];
            }
            if ([currentObj valueForKey:@"synonym4"]!= nil)
            {
                
                synonym4.text = [currentObj valueForKey:@"synonym4"];
                howManySyns.text = @"input 4";
                [self positionTheNodesOutsideOfTheBoxes];
            }
            //words
            if ([currentObj valueForKey:@"word1"]!= nil) {
                words1.text = [currentObj valueForKey:@"word1"];
                [self positionTheNodesOutsideOfTheBoxes];
            }
            if ([currentObj valueForKey:@"word2"]!= nil)
            {
                words2.text = [currentObj valueForKey:@"word2"];
                [self positionTheNodesOutsideOfTheBoxes];
            }
            if ([currentObj valueForKey:@"word3"]!= nil)
            {
                words3.text = [currentObj valueForKey:@"word3"];
                [self positionTheNodesOutsideOfTheBoxes];
            }
            if ([currentObj valueForKey:@"word4"]!= nil)
            {
                
                words4.text = [currentObj valueForKey:@"word4"];
                [self positionTheNodesOutsideOfTheBoxes];
            }
            if ([currentObj valueForKey:@"word5"]!= nil)
            {
                
                words5.text = [currentObj valueForKey:@"word5"];
                [self positionTheNodesOutsideOfTheBoxes];
            }
            //   }
        }
    }
    
}
-(void)removeTheOldValuesOfTheNodes{
    howManySyns.text = @"";
    howManyAnts.text = @"";
    words1.text = @"";
    words2.text = @"";
    words3.text = @"";
    words4.text = @"";
    words5.text = @"";
    antonym1.text = @"";
    antonym2.text = @"";
    antonym3.text = @"";
    antonym4.text = @"";
    synonym1.text = @"";
    synonym2.text = @"";
    synonym3.text = @"";
    synonym4.text = @"";
    title.text = @"";
    howManySyns = nil;
    howManyAnts= nil;
    words1= nil;
    words2= nil;
    words3= nil;
    words4= nil;
    words5= nil;
    antonym1= nil;
    antonym2= nil;
    antonym3= nil;
    antonym4= nil;
    synonym1= nil;
    synonym2= nil;
    synonym3= nil;
    synonym4= nil;
    title= nil;
  
}
-(void)removeTheLabelNodesFromTheBoxesAntonyms{
  
    //TODO: need to enumerate through the ants and the syns and the words nodes
    
}
-(void)addTheOutsideOfTheBoxesNodes{
    outsideOfTheBoxesLeft1 = nil;
    outsideOfTheBoxesLeft2 = nil;
    outsideOfTheBoxesLeft3 = nil;
    outsideOfTheBoxesLeft4 = nil;
    outsideOfTheBoxesLeft5 = nil;
    outsideOfTheBoxesMiddle1 = nil;
    outsideOfTheBoxesMiddle2 = nil;
    outsideOfTheBoxesMiddle3 = nil;
    outsideOfTheBoxesMiddle4 = nil;
    outsideOfTheBoxesMiddle5 = nil;
    outsideOfTheBoxesRight1 = nil;
    outsideOfTheBoxesRight2 = nil;
    outsideOfTheBoxesRight3 = nil;
    outsideOfTheBoxesRight4 = nil;
    outsideOfTheBoxesRight5 = nil;
    //sknode
    outsideOfTheBoxesLeft1 = [SKNode node];
    outsideOfTheBoxesLeft2 = [SKNode node];
    outsideOfTheBoxesLeft3 = [SKNode node];
    outsideOfTheBoxesLeft4 = [SKNode node];
    outsideOfTheBoxesLeft5 = [SKNode node];
    outsideOfTheBoxesMiddle1 = [SKNode node];
    outsideOfTheBoxesMiddle2 = [SKNode node];
    outsideOfTheBoxesMiddle3 = [SKNode node];
    outsideOfTheBoxesMiddle4 = [SKNode node];
    outsideOfTheBoxesMiddle5 = [SKNode node];
    outsideOfTheBoxesRight1 = [SKNode node];
    outsideOfTheBoxesRight2 = [SKNode node];
    outsideOfTheBoxesRight3 = [SKNode node];
    outsideOfTheBoxesRight4 = [SKNode node];
    outsideOfTheBoxesRight5 = [SKNode node];
    //name
    outsideOfTheBoxesLeft1.name = nil;
    outsideOfTheBoxesLeft2.name = nil;
    outsideOfTheBoxesLeft3.name = nil;
    outsideOfTheBoxesLeft4.name = nil;
    outsideOfTheBoxesLeft5.name = nil;
    outsideOfTheBoxesMiddle1.name = nil;
    outsideOfTheBoxesMiddle2.name = nil;
    outsideOfTheBoxesMiddle3.name = nil;
    outsideOfTheBoxesMiddle4.name = nil;
    outsideOfTheBoxesMiddle5.name = nil;
    outsideOfTheBoxesRight1.name = nil;
    outsideOfTheBoxesRight2.name = nil;
    outsideOfTheBoxesRight3.name = nil;
    outsideOfTheBoxesRight4.name = nil;
    outsideOfTheBoxesRight5.name = nil;
}
-(void)addTheInsideBoxesNodes{
    insideSynonym1 = nil;
    insideSynonym2 = nil;
    insideSynonym3 = nil;
    insideSynonym4 = nil;
    insideAntonym1 = nil;
    insideAntonym2 = nil;
    insideAntonym3 = nil;
    insideAntonym4 = nil;
    insideSynonym1 = [SKNode node];
    insideSynonym2 = [SKNode node];
    insideSynonym3 = [SKNode node];
    insideSynonym4 = [SKNode node];
    insideAntonym1 = [SKNode node];
    insideAntonym2 = [SKNode node];
    insideAntonym3 = [SKNode node];
    insideAntonym4 = [SKNode node];
    insideSynonym1.name = nil;
    insideSynonym2.name = nil;
    insideSynonym3.name = nil;
    insideSynonym4.name = nil;
    insideAntonym1.name = nil;
    insideAntonym2.name = nil;
    insideAntonym3.name = nil;
    insideAntonym4.name = nil;
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        
        node = [self nodeAtPoint:location];
        
        if ([node.name isEqualToString:@"finish"]) {
            node = nil;
            CurrentProgressOfGame *progressScene = [CurrentProgressOfGame sceneWithSize:self.size];
            SKTransition *myTransition = [SKTransition doorsOpenVerticalWithDuration:1];
            [self.view presentScene:progressScene transition:myTransition];
            }
         
        
        else if (([node.name isEqualToString:@"antonymName"] || [node.name isEqualToString:@"synonymName"] || [node.name isEqualToString:@"timeLabel"] ||
             [node.name isEqualToString:@"synonymBox"] ||
             [node.name isEqualToString:@"antonymBox"]  ||
             [node.name isEqualToString:@"title"] ||
             [node.name isEqualToString:@"finish"] ||
             [node.name isEqualToString:@"outOfTheBoxes"] ||
             [node.name isEqualToString:@"howManyAnts"] ||
             [node.name isEqualToString:@"howManySyns"])) {
            node = nil;
        }
       
        if(node) {
            [self removeTheLabelFromTheBoxes];
        }
        
    }
}
-(void)removeTheLabelFromTheBoxes{
    if ([antonymBox containsPoint:node.position]) {
        
        if (insideAntonym1.name == node.name) {
            node.position = [self moveTheNodeFromTheBoxes];
            insideAntonym1.name = nil;;
        }
        else if (insideAntonym2.name == node.name) {
            node.position = [self moveTheNodeFromTheBoxes];            insideAntonym2.name = nil;
        }
        else if (insideAntonym3.name == node.name) {
            node.position = [self moveTheNodeFromTheBoxes];
            insideAntonym3.name = nil;
        }
        else if (insideAntonym4.name == node.name) {
            node.position = [self moveTheNodeFromTheBoxes];            insideAntonym4.name = nil;
        }
        //node = nil;
    }
    else if ([synonymBox containsPoint:node.position]) {
        
        if (insideSynonym1.name == node.name) {
            node.position = [self moveTheNodeFromTheBoxes];
            insideSynonym1.name = nil;
        }
        else if (insideSynonym2.name == node.name) {
            node.position = [self moveTheNodeFromTheBoxes];
            insideSynonym2.name = nil;
        }
        else if (insideSynonym3.name == node.name) {
            node.position = [self moveTheNodeFromTheBoxes];
            insideSynonym3.name = nil;
        }
        else if (insideSynonym4.name == node.name) {
            node.position = [self moveTheNodeFromTheBoxes];
            insideSynonym4.name = nil;
        }
        //node = nil;
    }
}
-(void)positionTheNodesOutsideOfTheBoxes{
    //if ([outOfTheBoxes containsPoint:node.position]) {
        float outOfTheBoxesNodePos = screenWidth/3;
        float outOfTheBoxesNodePosMiddle = screenWidth/3 * 2;
        float outOfTheBoxesNodePosRight = screenWidth/4 * 3;
        if (outsideOfTheBoxesLeft1.name == nil) {
            node.position = CGPointMake(outOfTheBoxesNodePos, screenHeight/2 - 20);
            outsideOfTheBoxesLeft1.name = @"pos1";
        }
        else if (outsideOfTheBoxesLeft2.name == nil) {
            node.position = CGPointMake(outOfTheBoxesNodePos, screenHeight/2 - 30);
            outsideOfTheBoxesLeft2.name = @"pos2";
        }
        else if (outsideOfTheBoxesLeft3.name == nil) {
            node.position = CGPointMake(outOfTheBoxesNodePos, screenHeight/2 - 60);
            outsideOfTheBoxesLeft3.name = @"pos3";
        }
        else if (outsideOfTheBoxesLeft4.name == nil) {
            node.position = CGPointMake(outOfTheBoxesNodePos, screenHeight/2 - 90);
            outsideOfTheBoxesLeft4.name = @"pos4";
        }
        else if (outsideOfTheBoxesLeft5.name == nil) {
            node.position = CGPointMake(outOfTheBoxesNodePos, screenHeight/2 - 120);
            outsideOfTheBoxesLeft5.name = @"pos5";
        }
        //outsideOfTheBoxesMiddle
        
        else if (outsideOfTheBoxesMiddle1.name == nil) {
            node.position = CGPointMake(outOfTheBoxesNodePosMiddle  , screenHeight/2 - 10);
            outsideOfTheBoxesMiddle1.name = @"pos6";
        }
        else if (outsideOfTheBoxesMiddle2.name == nil) {
            node.position = CGPointMake(outOfTheBoxesNodePosMiddle, screenHeight/2 - 30);
            outsideOfTheBoxesMiddle2.name = @"pos7";
        }
        else if (outsideOfTheBoxesMiddle3.name == nil) {
            node.position = CGPointMake(outOfTheBoxesNodePosMiddle, screenHeight/2 - 60);
            outsideOfTheBoxesMiddle3.name = @"pos8";
        }
        else if (outsideOfTheBoxesMiddle4.name == nil) {
            node.position = CGPointMake(outOfTheBoxesNodePosMiddle, screenHeight/2 - 90);
            outsideOfTheBoxesMiddle4.name = @"pos9";
        }
        else if (outsideOfTheBoxesMiddle5.name == nil) {
            node.position = CGPointMake(outOfTheBoxesNodePosMiddle, screenHeight/2 - 120);
            outsideOfTheBoxesMiddle5.name = @"pos10";
        }
        //outsideOfTheBoxesRight
        
        else if (outsideOfTheBoxesRight1.name == nil) {
            node.position = CGPointMake(outOfTheBoxesNodePosRight  , screenHeight/2 - 10);
            outsideOfTheBoxesRight1.name = @"pos11";
        }
        else if (outsideOfTheBoxesRight2.name == nil) {
            node.position = CGPointMake(outOfTheBoxesNodePosRight, screenHeight/2 - 30);
            outsideOfTheBoxesRight2.name = @"pos12";
        }
        else if (outsideOfTheBoxesRight3.name == nil) {
            node.position = CGPointMake(outOfTheBoxesNodePosRight, screenHeight/2 - 60);
            outsideOfTheBoxesRight3.name = @"pos13";
        }
//        else if (outsideOfTheBoxesRight4.name == nil) {
//            node.position = CGPointMake(outOfTheBoxesNodePosRight, screenHeight/2 - 90);
//            outsideOfTheBoxesRight4.name = @"pos14";
//        }
//        else if (outsideOfTheBoxesRight5.name == nil) {
//            node.position = CGPointMake(outOfTheBoxesNodePosRight, screenHeight/2 - 120);
//            outsideOfTheBoxesRight5.name = @"pos15";
//        }
   // }
}
-(CGPoint)moveTheNodeFromTheBoxes{
//    int posWidth = (arc4random() % (int)(screenWidth - 70))+30;
//    int posHeight = (arc4random() % (int)(screenHeight/2 - 80))+30;
//    node.position = CGPointMake(posWidth, posHeight);
    [self positionTheNodesOutsideOfTheBoxes];
    return node.position;
}
#pragma mark - reviewIfTouchedOutOfTheBoxes
-(void)reviewIfTouchedOutOfTheBoxes{
    if ([outOfTheBoxes containsPoint:node.position] && (![antonymBox containsPoint:node.position] && ![synonymBox containsPoint:node.position])) {
        NSLog(@"outoftheBoxes position of node");
        [self moveTheNodeFromTheBoxes];
    }
}
-(void)removeNodeFromOutsideOfTheBoxes{
    [[self children] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
    SKNode *node1 = (SKNode *)obj;
        NSLog(@"&& node1.name %@", node1.name);
        NSLog(@"&& node.name %@", node.name);
        if ([node1.name isEqualToString:node.name]) {
      
            NSLog(@"**<< node1.name = %@ && intersects antonymBox or synonymBox", node1.name);
            
            NSLog(@"[self nodeAtPoint:node1.position].name = %@", [self nodeAtPoint:node1.position].name);
            node1.name = nil;
        }
//            NSArray *buttonNodes = [self nodesAtPoint:node.position];
//            for (SKNode *node2 in buttonNodes)
//            {
//                NSLog(@"** [self nodeAtPoint:node2.position] = %@", [self nodeAtPoint:node2.position]);
//                [self nodeAtPoint:node2.position].name = nil;
//            //}
            
        
        
    }
     ];
    
//    [self enumerateChildNodesWithName:@"syn*" usingBlock:^(SKNode*node1, BOOL *stop){
//        if(node1 == [self nodeAtPoint:node.position]){
//            NSLog(@"**>> [self nodeAtPoint:node.position].name = %@ && node.name = %@", [self nodeAtPoint:node.position].name, node.name);
//           // NSArray *buttonNodes = [self nodesAtPoint:node.position];
////            for (SKNode *node2 in buttonNodes)
////            {
////                NSLog(@"node2.name = %@", node2.name);
////                [self nodeAtPoint:node2.position].name = nil;
////            }
//            
//        }
//        
//    }
//     ];
//
//    [self enumerateChildNodesWithName:@"ant*" usingBlock:^(SKNode*node1, BOOL *stop){
//        
//        if(node1 == [self nodeAtPoint:node.position]){
//            NSLog(@"**// [self nodeAtPoint:node.position] = %@ && node.name = %@", [self nodeAtPoint:node.position], node.name);
//          //  NSArray *buttonNodes = [self nodesAtPoint:node.position];
////            for (SKNode *node2 in buttonNodes)
////            {
////                NSLog(@"node2.name = %@", node2.name);
////                [self nodeAtPoint:node2.position].name = nil;
////            }
//            
//        }
//        
//    }
//     ];

}
#pragma mark - reviewThePositionOfLabel
-(void)reviewThePositionOfLabelInsideTheBoxes{
    
        if ([antonymBox containsPoint:node.position]) {
            float antPosBox = antonymBox.position.x + (screenWidth/2 - 10)/2;
      
            if (insideAntonym1.name == nil) {
                node.position = CGPointMake(antPosBox, screenHeight/2 + 3);
                insideAntonym1.name = node.name;
                [self removeNodeFromOutsideOfTheBoxes];
            }
            else if (insideAntonym2.name == nil) {
                node.position = CGPointMake(antPosBox, screenHeight/2 + 30);
                insideAntonym2.name = node.name;
                [self removeNodeFromOutsideOfTheBoxes];
            }
            else if (insideAntonym3.name == nil) {
                node.position = CGPointMake(antPosBox, screenHeight/2 + 60);
                insideAntonym3.name = node.name;
                [self removeNodeFromOutsideOfTheBoxes];
            }
            else if (insideAntonym4.name == nil) {
                node.position = CGPointMake(antPosBox, screenHeight/2 + 90);
                insideAntonym4.name = node.name;
                [self removeNodeFromOutsideOfTheBoxes];
            }
            else{
                
                [self removeNodeFromOutsideOfTheBoxes];
                 [self positionTheNodesOutsideOfTheBoxes];
//                int posWidth = (arc4random() % (int)(screenWidth - 70))+30;
//                int posHeight = (arc4random() % (int)(screenHeight/2 - 80))+30;
//                node.position = CGPointMake(posWidth, posHeight);
            }
        }
        else if ([synonymBox containsPoint:node.position]) {
           float synPosBox = (screenWidth/2 +10) + (screenWidth/2 - 20)/2;
            
            if (insideSynonym1.name == nil) {
                node.position = CGPointMake(synPosBox, screenHeight/2 + 3);
                insideSynonym1.name = node.name;
                [self removeNodeFromOutsideOfTheBoxes];
            }
            else if (insideSynonym2.name == nil) {
                node.position = CGPointMake(synPosBox, screenHeight/2 + 30);
                insideSynonym2.name = node.name;
                [self removeNodeFromOutsideOfTheBoxes];
            }
            else if (insideSynonym3.name == nil) {
                node.position = CGPointMake(synPosBox, screenHeight/2 + 60);
                insideSynonym3.name = node.name;
                [self removeNodeFromOutsideOfTheBoxes];
            }
            else if (insideSynonym4.name == nil) {
                node.position = CGPointMake(synPosBox, screenHeight/2 + 90);
                insideSynonym4.name = node.name;
                [self removeNodeFromOutsideOfTheBoxes];
            }
            else{
                [self removeNodeFromOutsideOfTheBoxes];
                [self positionTheNodesOutsideOfTheBoxes];
//                int posWidth = (arc4random() % (int)(screenWidth - 70))+30;
//                int posHeight = (arc4random() % (int)(screenHeight/2 - 80))+30;
//                node.position = CGPointMake(posWidth, posHeight);
            }
        }
    
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    if (node) {
        if(!myQueue){
           myQueue = dispatch_queue_create("myQueueName", NULL);
        }
        dispatch_async(myQueue, ^{[self reviewThePositionOfLabelInsideTheBoxes];});
        if (!myQueue2) {
            myQueue2 = dispatch_queue_create("ruleonsix.co.uk", NULL);
        }
        dispatch_async(myQueue2, ^{[self reviewIfTouchedOutOfTheBoxes];});
        
    
    }
}
-(void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{
    if (node) {
        if(!myQueue){
            myQueue = dispatch_queue_create("myQueueName", NULL);
        }
        dispatch_async(myQueue, ^{[self reviewThePositionOfLabelInsideTheBoxes];});
        if (!myQueue2) {
            myQueue2 = dispatch_queue_create("ruleonsix.co.uk", NULL);
        }
        dispatch_async(myQueue2, ^{[self reviewIfTouchedOutOfTheBoxes];});
        
    }
}
-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        if (node.name != nil) {
            node.position = location;
            float pos = location.y - screenHeight/2;
            positionLabel.text = [NSString stringWithFormat:@"pos half height: %f", pos];
        }
        
    }
}
- (void)updateWithTimeSinceLastUpdate:(CFTimeInterval)timeSinceLast {
    
    self.lastSpawnTimeInterval += timeSinceLast;
    if (self.lastSpawnTimeInterval > 1) {
        self.lastSpawnTimeInterval = 0;
        timeLabel.text = [NSString stringWithFormat:@"%d",timeCount--];
    }
}
-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    // Handle time delta.
    // If we drop below 60fps, we still want everything to move the same distance.
    CFTimeInterval timeSinceLast = currentTime - self.lastUpdateTimeInterval;
    self.lastUpdateTimeInterval = currentTime;
    if (timeSinceLast > 1) { // more than a second since last update
        timeSinceLast = 1.0 / 60.0;
        self.lastUpdateTimeInterval = currentTime;
    }
    
    [self updateWithTimeSinceLastUpdate:timeSinceLast];
    
}

@end
