//
//  GameScene.h
//  Syn
//

//  Copyright (c) 2014 RuleOnSix. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <CoreData/CoreData.h>
@interface GameScene : SKScene<NSFetchedResultsControllerDelegate>
{
    NSFetchedResultsController *fetchedResultsController;
    NSManagedObjectContext *managedObjectContext;
    
}
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic) NSTimeInterval lastSpawnTimeInterval;
@property (nonatomic) NSTimeInterval lastUpdateTimeInterval;
//-(void)moveTheCurrentCardNumberUpByOne;
@end
