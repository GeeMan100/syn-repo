//
//  main.m
//  Syn
//
//  Created by Gurmit Singh on 23/08/2014.
//  Copyright (c) 2014 RuleOnSix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
