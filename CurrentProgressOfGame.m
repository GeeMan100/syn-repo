//
//  CurrentProgressOfGame.m
//  Syn
//
//  Created by Gurmit Singh on 01/09/2014.
//  Copyright (c) 2014 RuleOnSix. All rights reserved.
//
#import "GameScene.h"
#import "CurrentProgressOfGame.h"

@implementation CurrentProgressOfGame
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    GameScene *game = [GameScene sceneWithSize:self.size];
    SKTransition *myGameTransition = [SKTransition doorsOpenHorizontalWithDuration:1.5];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"myCurrentGame"] == nil) {
        [defaults setObject:@"0" forKey:@"myCurrentGame"];
        [defaults synchronize];
    }
    
    NSString *getValue = [defaults objectForKey:@"myCurrentGame"];
    int defaultsValue = [getValue intValue];
    defaultsValue = defaultsValue + 1;
    [defaults setObject:[NSString stringWithFormat:@"%d",defaultsValue] forKey:@"myCurrentGame"];
    
    [defaults synchronize];
    NSLog(@"**** defaultsValue = %d", defaultsValue);
   // currentNumber = [getValue intValue];
    [self.view presentScene:game transition:myGameTransition];
}
@end
